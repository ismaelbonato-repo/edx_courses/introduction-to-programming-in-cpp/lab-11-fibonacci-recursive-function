# First lab from Introduction to Programming in C++ course on EDX.

# How to compile

```
mkdir buid && cd build
cmake ..
make

or

g++ -o FibonacciLab evennumbers.cpp
```
